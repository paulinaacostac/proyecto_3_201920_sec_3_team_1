package model.logic;

import java.awt.geom.Area;
import java.io.BufferedReader;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Iterator;

import com.google.gson.Gson;
import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.Esquina;
import model.data_structures.Graph;
import model.data_structures.Edge;
import model.data_structures.IndexHeap;
import model.data_structures.LinearProbingHashST;
import model.data_structures.MaxHeapCP;
import model.data_structures.MinHeap;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.UnionFind;
import model.data_structures.Vertex;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo
{
	/**
	 * Atributos del modelo del mundo
	 */
	private int numeroArcos;
	private Graph<Integer, Vertice> grafo;
	private Graph<Integer, Vertice> grafoZonas;
	private ArregloDinamico<Arco> arcos;
	private ArregloDinamico<Vertice> vertices;
	private ArregloDinamico<Viaje> viajes;
	private int numViajes;
	private LinearProbingHashST<String, Viaje> STLinear;
	private MaxHeapCP<Double> pqDijkstra;
	private LinearProbingHashST<Integer,Edge> edgeTo;
	private LinearProbingHashST<Integer,Double> distTo;

	// Extra
	private int contadorKruskal=0;
	private double costoTotalKruskal=0.0;
	private int contadorPrim=0;
	private double costoTotalPrim=0.0;
	private boolean print = true;
	// 
	private int arcos1A = 0;
	private double costoTiempo1A = 0;
	private double costoDistancia1A = 0;
	private int arcos1B = 0;
	private double costoTiempo1B = 0;
	private double costoDistancia1B = 0;
	private int arcos2C = 0;
	private double costoTiempo2C=0;
	private int arcos3C;
	
	//	private IndexHeap<Double> pq;

	public MVCModelo()
	{
		STLinear=new LinearProbingHashST<String,Viaje>(200000);
		arcos=new ArregloDinamico<Arco>(1000);
		vertices=new ArregloDinamico<Vertice>(1000);
		viajes=new ArregloDinamico<Viaje>(1000);
		grafo=new Graph<Integer,Vertice>();
		pqDijkstra=new MaxHeapCP<Double>(1000);
		edgeTo=new LinearProbingHashST<Integer, Edge>();
		distTo=new LinearProbingHashST<Integer, Double>();
		//		pq=new IndexHeap<Double>(200000);
		numeroArcos=0;
		numViajes=0;

	}
	public void exportarGrafoDelimitado(double longMin, double longMax, double LatMin, double LatMax,String ruta)
	{
		PrintWriter writer;
		int Numarcos=0;
		int Numvertices=0;
		ArregloDinamico<Vertice> verticesEnZona=new ArregloDinamico<Vertice>(7000);
		ArregloDinamico<Arco> arcosEnZona=new ArregloDinamico<Arco>(7000);
		try 
		{
			writer = new PrintWriter(ruta, "UTF-8");
			writer.println("{\"type\":\"FeatureCollection\",\"features\":[");

			//Exportar Arcos
			for (int i=0;i<grafo.E();i++)
			{
				int idV1=arcos.darElemento(i).darVerticesAdyacentes().darElemento(0);
				int idV2=arcos.darElemento(i).darVerticesAdyacentes().darElemento(1);
				Vertice v1=grafo.getInfoVertex(idV1);
				Vertice v2=grafo.getInfoVertex(idV2);
				if (v1!=null &&v2!=null&&v1.darLatitud()>=LatMin && v1.darLatitud()<=LatMax && v1.darLongitud()>=longMin && v1.darLongitud()<=longMax && v2.darLatitud()>=LatMin && v2.darLatitud()<=LatMax && v2.darLongitud()>=longMin && v2.darLongitud()<=longMax)
				{
					arcosEnZona.agregar(arcos.darElemento(i));
				}
			}
			for (int i=0;i<arcosEnZona.darTamano();i++)
			{
				Arco a=arcosEnZona.darElemento(i);
				int idV1=arcosEnZona.darElemento(i).darVerticesAdyacentes().darElemento(0);
				int idV2=arcosEnZona.darElemento(i).darVerticesAdyacentes().darElemento(1);
				Vertice v1=grafo.getInfoVertex(idV1);
				Vertice v2=grafo.getInfoVertex(idV2);
				Numarcos++;
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia|id\":"+arcos.darElemento(i).darDistancia()+", \"idV1|zona\":"+idV1+", \"idv2\":"+idV2+", \"tiempo\":"+a.darTiempo()+", \"velocidad\":"+a.darVelocidad()+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+v1.darLongitud()+","+v1.darLatitud()+"],["+v2.darLongitud()+","+v2.darLatitud()+"]],\"type\":\"LineString\"}");
				writer.println("},");
			}


			//Exportar vertices			
			for (int i=0;i<grafo.V();i++)
			{
				Vertice v=vertices.darElemento(i);
				if (v.darLatitud()>=LatMin && v.darLatitud()<=LatMax && v.darLongitud()>=longMin && v.darLongitud()<=longMax)
				{
					verticesEnZona.agregar(v);
				}
			}
			for (int i=0;i<verticesEnZona.darTamano();i++)
			{
				Numvertices++;
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia|id\":"+verticesEnZona.darElemento(i).darId()+", \"idV1|zona\":"+verticesEnZona.darElemento(i).darZona()+", \"idv2\":"+"null"+", \"tiempo\":"+"null"+", \"velocidad\":"+"null"+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+verticesEnZona.darElemento(i).darLongitud()+","+verticesEnZona.darElemento(i).darLatitud()+"],["+"null"+","+"null"+"]],"+"\"type\":\"Point\"}");
				if (i!=verticesEnZona.darTamano()-1)
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}
			writer.println("]}");
			writer.close();
			System.out.println("Vertices exportados en grafo delimitado: "+Numvertices);
			System.out.println("Arcos exportados en grafo delimitado: "+Numarcos);
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int darContadorKruskal()
	{
		return contadorKruskal;
	}
	public double darCostoTotalKruskal()
	{
		return costoTotalKruskal;
	}
	public int darContadorPrim()
	{
		return contadorPrim;
	}
	public double darCostoTotalPrim()
	{
		return costoTotalPrim;
	}
	
	public double darCostoDistancia1A()
	{
		return costoDistancia1A;
	}
	
	public double darCostoDistancia1B()
	{
		return costoDistancia1B;
	}
	
	public double darCostoTiempo1A()
	{
		return costoTiempo1A;
	}
	
	public double darCostoTiempo1B()
	{
		return costoTiempo1B;
	}
	public double darCostoTiempo2C()
	{
		return costoTiempo2C;
	}
	public int darArcos1A()
	{
		return arcos1A;
	}
	public int darArcos1B()
	{
		return arcos1B;
	}
	public int darArcos2C()
	{
		return arcos2C;
	}

	public void exportarCamino(Stack<Edge> path,String ruta)
	{
		PrintWriter writer;
		try 
		{
			//Exportar arcos
			writer = new PrintWriter(ruta+".json", "UTF-8");
			writer.println("{");
			writer.println("\"type\":\"FeatureCollection\",");
			writer.println("\"features\":[");
			while (!path.isEmpty()) 
			{
				Edge e = path.pop();
				Vertice vert =(Vertice)e.either().getInfo();
				int id = vert.darId();
				Vertice vert2 =(Vertice)e.other(e.either()).getInfo();
				int id2 = vert2.darId();
				if(print) System.out.println(id + "-" +id2);
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia|id\":"+e.getCost()+", \"idv1|zona\":"+id+", \"idv2\":"+id2+", \"tiempo\":"+e.getTiempo()+", \"velocidad\":"+e.getVelocidad()+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+vert.darLongitud()+","+vert.darLatitud()+"],["+vert2.darLongitud()+","+vert2.darLatitud()+"]],\"type\":\"LineString\"}");
				writer.println("},");
				//Vertices
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia|id\":"+id+", \"idv1|zona\":"+vert.darZona()+", \"tiempo\":"+"null"+", \"velocidad\":"+"null"+"},");
				writer.println("\"geometry\":{\"coordinates\": ["+vert.darLongitud()+","+vert.darLatitud()+"],\"type\":\"Point\"}");
				writer.println("},");
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia|id\":"+id2+", \"idv1|zona\":"+vert2.darZona()+", \"tiempo\":"+"null"+", \"velocidad\":"+"null"+"},");
				writer.println("\"geometry\":{\"coordinates\": ["+vert2.darLongitud()+","+vert2.darLatitud()+"],\"type\":\"Point\"}");
				if (!path.isEmpty())
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}

			writer.println("]}");
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void exportarHash(LinearProbingHashST<Integer, Double> hash,String ruta)
	{
		PrintWriter writer;
		try 
		{
			//Exportar arcos
			writer = new PrintWriter(ruta+".json", "UTF-8");
			writer.println("{");
			writer.println("\"type\":\"FeatureCollection\",");
			writer.println("\"features\":[");
			Iterator<Integer> iter=hash.keys();
			while (iter.hasNext()) 
			{
				int id=iter.next();
				Vertice v=(Vertice) grafo.getVertex(id).getInfo();
				//Vertices
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				//writer.println("\"properties\":{\"distancia|id\":"+id+", \"idv1|zona\":"+v.darZona()+", \"tiempo\":"+"null"+", \"velocidad\":"+"null"+", \"color\":"+"\"#F7455D\""+"},");
				writer.println("\"properties\":{\"distancia|id\":"+id+", \"idv1|zona\":"+v.darZona()+", \"tiempo\":"+"null"+", \"velocidad\":"+"null"+"},");
				writer.println("\"geometry\":{\"coordinates\": ["+v.darLongitud()+","+v.darLatitud()+"],\"type\":\"Point\"}");
				if (iter.hasNext())
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}

			writer.println("]}");
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void exportarGrafo(String ruta)
	{
		PrintWriter writer;
		try 
		{
			//Exportar arcos
			writer = new PrintWriter(ruta, "UTF-8");
			writer.println("{\"type\":\"FeatureCollection\",\"features\":[");
			for (int i=0;i<grafo.E();i++)
			{
				Arco arc=arcos.darElemento(i);
				int idV1=arcos.darElemento(i).darVerticesAdyacentes().darElemento(0);
				int idV2=arcos.darElemento(i).darVerticesAdyacentes().darElemento(1);
				Vertice v1=grafo.getInfoVertex(idV1);
				Vertice v2=grafo.getInfoVertex(idV2);
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia|id\":"+arc.darDistancia()+", \"idv1|zona\":"+idV1+", \"idv2\":"+idV2+", \"tiempo\":"+arc.darTiempo()+", \"velocidad\":"+arc.darVelocidad()+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+v1.darLongitud()+","+v1.darLatitud()+"],["+v2.darLongitud()+","+v2.darLatitud()+"]],\"type\":\"LineString\"}");
				writer.println("},");
			}

			//Exportar vertices
			for (int i=0;i<grafo.V();i++)
			{
				writer.println("{");
				writer.println("\"type\":\"Feature\",");
				writer.println("\"properties\":{\"distancia|id\":"+vertices.darElemento(i).darId()+", \"idv1|zona\":"+vertices.darElemento(i).darZona()+", \"tiempo\":"+"null"+", \"velocidad\":"+"null"+"},");
				writer.println("\"geometry\":{\"coordinates\": [["+vertices.darElemento(i).darLongitud()+","+vertices.darElemento(i).darLatitud()+"],["+"null"+","+"null"+"]],\"type\":\"Point\"}");
				if (i!=grafo.V()-1)
				{
					writer.println("},");
				}
				else
					writer.println("}");
			}
			writer.println("]}");
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void importarGrafo(String ruta)
	{
		Graph<Integer, Vertice> g=new Graph<Integer,Vertice>();
		ArregloDinamico<Vertice> vs=new ArregloDinamico<Vertice>(200000);
		ArregloDinamico<Arco> as=new ArregloDinamico<Arco>(200000);
		Gson gson = new Gson();
		try (Reader reader = new FileReader(ruta)) 
		{
			ArregloJSON az=gson.fromJson(reader, ArregloJSON.class);
			//			System.out.println(az.darFeatures()[0].getProperties().toString());
			for (int i=0;i<az.darFeatures().length;i++)
			{
				String tipoFeature=az.darFeatures()[i].getGeometry().darTipo();
				if (tipoFeature.equals("Point"))
				{
					//Importar vertices
					int id=(int) az.darFeatures()[i].getProperties().darDistanciaoId();
					int zona=az.darFeatures()[i].getProperties().daridV1oZona();
					double latitud=Double.valueOf(az.darFeatures()[i].getGeometry().getCoordinates()[0][0]);
					double longitud=Double.valueOf(az.darFeatures()[i].getGeometry().getCoordinates()[0][1]);
					Vertice v=new Vertice(id,longitud,latitud,zona);
					vs.agregar(v);
					g.addVertex(id, v);
				}
			}
			for (int i=0;i<az.darFeatures().length;i++)
			{
				String tipoFeature=az.darFeatures()[i].getGeometry().darTipo();
				if (tipoFeature.equals("LineString"))
				{
					//Importar Arcos
					int v1=az.darFeatures()[i].getProperties().daridV1oZona();
					int v2=az.darFeatures()[i].getProperties().daridV2();
					ArregloDinamico<Integer> verticesAdyacentes=new ArregloDinamico<Integer>(2);
					verticesAdyacentes.agregar(v1);
					verticesAdyacentes.agregar(v2);
					double distancia=az.darFeatures()[i].getProperties().darDistanciaoId();
					double tiempo=az.darFeatures()[i].getProperties().darTiempo();
					double velocidad=az.darFeatures()[i].getProperties().darVelocidad();
					as.agregar(new Arco(verticesAdyacentes,distancia, tiempo, velocidad));
					g.addEdge(v1, v2, distancia,tiempo,velocidad);
				}
			}
			grafo=g;
			vertices=vs;
			arcos=as;
			//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLatitud()+" Latitud");
			//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLongitud()+" Longitud");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void cargarVertices()
	{
		File file = new File("./data/bogota_vertices.txt");
		BufferedReader br = null;
		String nextLine="";
		try 
		{
			br = new BufferedReader(new FileReader(file));
			nextLine=br.readLine(); //saltar linea de nombre de columnas
			while ((nextLine = br.readLine()) != null)
			{
				String partes[]=nextLine.split(";");
				Vertice vertice = new Vertice(Integer.parseInt(partes[0]), Double.parseDouble(partes[1]), Double.parseDouble(partes[2]), Integer.parseInt(partes[3])); 
				vertices.agregar(vertice);
				grafo.addVertex(Integer.parseInt(partes[0]), vertice);
				//System.out.println(vertice.toString());//esquinas.agregar(esquina);
			}
			System.out.println("vertices cargados");
		}
		catch (NumberFormatException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cargarViajes()
	{
		CSVReader reader = null;
		Viaje viaje=null;
		int c=0;
		try 
		{
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-AllWeeklyAggregate.csv"));
			//reader = new CSVReader(new FileReader("./data/pruebaViajes.txt"));
			reader.skip(1);
			String keyViaje=null;
			//Trimestre 1
			for(String[] nextLine : reader) 
			{
				viaje = new Viaje(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				keyViaje=nextLine[0]+"-"+ nextLine[1];
				STLinear.putInSet(keyViaje, viaje);
				c++;
			}
			System.out.println("viajes cargados");

		}
		catch (FileNotFoundException e)  
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//	private boolean hayDuplicado(Viaje viaje) 
	//	{
	//		boolean r=false;
	//		for (int i=0;i<viajes.darTamano();i++)
	//		{
	//			Viaje v=viajes.darElemento(i);
	//			if (v.darDstid()==viaje.darDstid() && v.darSourceid()==viaje.darSourceid())
	//			{
	//				v.cambiarTiempo(t);
	//				r=true;
	//			}
	//		}
	//		return r;
	//	}
	public double buscarTiempoArco(int id1,int id2)
	{
		double rta=0.0;
		boolean encontro=false;

		String s=id1+"-"+id2;
		Viaje v=STLinear.get(s);
		if (v!=null)
		{
			rta=v.darMeanTravelTime();
			encontro=true;
		}

		if (!encontro && id1==id2)
		{
			rta=10.0;
		}
		else if (!encontro)
		{
			rta=100.0;
		}

		return rta;
	}

	public void cargarArcos()
	{
		File file = new File("./data/bogota_arcos.txt");
		BufferedReader br = null;
		String nextLine="";
		String prueba="";
		int c=0;
		try 
		{
			br = new BufferedReader(new FileReader(file));
			while ((nextLine = br.readLine()) != null)
			{
				String partes[]=nextLine.split(" ");
				for (int i=0;i<partes.length-1;i++)
				{
					ArregloDinamico<Integer> verticesAdyacentes=new ArregloDinamico<Integer>(2);
					Vertice v=grafo.getInfoVertex(Integer.parseInt(partes[0]));
					Vertice v1=grafo.getInfoVertex(Integer.parseInt(partes[i+1]));
					if (v!=null && v1!=null)
					{
						double long1=v.darLongitud();
						double lat1=v.darLatitud();
						double long2=v1.darLongitud();
						double lat2=v1.darLatitud();
						Haversine h=new Haversine();
						double d=h.distance(lat1, long1, lat2, long2);
						verticesAdyacentes.agregar(Integer.parseInt(partes[0]));
						verticesAdyacentes.agregar(Integer.parseInt(partes[i+1]));
						double tiempo=buscarTiempoArco(v.darZona(), v1.darZona());
						arcos.agregar(new Arco(verticesAdyacentes,d,tiempo,d/tiempo));
						grafo.addEdge(v.darId(), v1.darId(), d,tiempo,d/tiempo);
					}
				}

			}
			System.out.println("Arcos cargados");
		}
		catch (NumberFormatException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int componentesConectadas()
	{
		grafo.uncheck();
		return grafo.cc();
	}
	public Iterator<Integer> dfsV0()
	{
		grafo.uncheck();
		Vertice v=grafo.getInfoVertex(0);
		Iterator<Integer> iter=grafo.getCC(v.darId());
		return iter;
	}
	public MaxHeapCP<Integer> darColaNumCCs()
	{
		grafo.uncheck();
		componentesConectadas();
		MaxHeapCP<Integer> cola=new MaxHeapCP<Integer>();
		Iterator<Integer> iter=grafo.numCC();
		LinearProbingHashST<Integer,Integer> st=new LinearProbingHashST<Integer,Integer>();
		int numrep=0;
		while (iter.hasNext())
		{
			int codigoCC=iter.next();
			if (st.get(codigoCC)!=null)
			{
				st.put(codigoCC, st.get(codigoCC)+1);
			}
			else
			{
				st.put(codigoCC, 1);
			}
		}
		Iterator<Integer> iter1=st.keys();
		while (iter1.hasNext())
		{
			cola.insertar(st.get(iter1.next()));
		}
		return cola;
	}
	public Graph<Integer,Vertice> darGrafo()
	{
		return grafo;
	}
	public LinearProbingHashST<Integer,Vertice> buscarVertice(double latitudOrigen, double longitudOrigen, double latitudDestino, double longitudDestino)
	{
		LinearProbingHashST<Integer, Vertice> ids=new LinearProbingHashST<Integer, Vertice>();
		for (int i=0;i<vertices.darTamano();i++)
		{
			Vertice v=vertices.darElemento(i);

			if (v.darLatitud()==latitudOrigen && v.darLongitud()==longitudOrigen)
			{
				ids.put(1, v);
			}
			if (v.darLatitud()==latitudDestino && v.darLongitud()==latitudDestino)
			{
				ids.put(2, v);
			}
		}
		return ids;
	}

	public Vertice verticeMasCercano(double pLongitud, double pLatitud)
	{
		Iterator<Vertice> vertices = grafo.getVertex();
		Vertice vertex = null;
		double min = Double.POSITIVE_INFINITY;
		while (vertices.hasNext()) 
		{
			Vertice vertice = (Vertice) vertices.next();
			double dist = Haversine.distance(pLatitud, pLongitud, vertice.darLatitud(), vertice.darLongitud());
			if (dist<min) {
				min = dist;
				vertex = vertice;
			}
		}
		return vertex;
	}

	public Stack<Edge> darCaminoCostoMinimoTiempo(double pLongitudOrigen, double pLatitudOrigen,double pLongitudDest, double pLatitudDest,String ruta)
	{
		Vertice origen = verticeMasCercano(pLongitudOrigen, pLatitudOrigen);
		System.out.println("El verice origen ID: " + origen.darId());
		Vertice dest = verticeMasCercano(pLongitudDest, pLatitudDest);
		System.out.println("El verice destino ID: " + dest.darId());
		LinearProbingHashST<Integer,Edge> edgeTo = new LinearProbingHashST<Integer, Edge>();
		LinearProbingHashST<Integer,Double> distTo = new LinearProbingHashST<Integer, Double>();
		IndexHeap<Double> pq = new IndexHeap<Double>(grafo.E());
		Iterator<Vertice> vertices = grafo.getVertex();
		while (vertices.hasNext()) 
		{
			Vertice vertice = (Vertice) vertices.next();
			distTo.put(vertice.darId(), Double.POSITIVE_INFINITY);
		}
		distTo.put(origen.darId(), 0.0);
		pq.insert(origen.darId(), 0.0);

		while (!pq.isEmpty()) 
		{
			//System.out.println("Entre");
			int id = pq.delMin();
			Vertex<Integer,Vertice> v = grafo.getVertex(id);
			ArregloDinamico<Edge> edges = grafo.adjEdges(id);
			for (int i = 0; i < edges.darTamano(); i++) 
			{
				//System.out.println("Entre x2");
				Edge e = edges.darElemento(i);
				Vertex<Integer,Vertice> w = e.other(v);
				if (distTo.get(w.getInfo().darId()) > distTo.get(v.getInfo().darId()) + e.getTiempo()) 
				{
					distTo.put(w.getInfo().darId(), distTo.get(v.getInfo().darId()) + e.getTiempo());
					edgeTo.put(w.getInfo().darId(), e);
					if (pq.contains(w.getInfo().darId())) pq.decreaseKey(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
					else                pq.insert(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
				}
			}
		}
		//System.out.println(distTo.get(dest.darId()));
		// Falta construir camino

		Stack<Edge> path = new Stack<Edge>();
		int x = dest.darId();
		System.out.println("Camino (ID | long | lat) -- (ID | long | lat)");
		for (Edge e = edgeTo.get(dest.darId()); e != null; e = edgeTo.get(x)) {
			//System.out.println("Entre x3");
			arcos1A++;
			costoDistancia1A+=e.darCosto();
			costoTiempo1A+=e.getTiempo();
			path.push(e);
			Vertex temp = grafo.getVertex(x);
			Vertice tempW = (Vertice) temp.getInfo();
			Vertice tempV = (Vertice) e.other(temp).getInfo();
			System.out.println(tempV.darId() +" | " + tempV.darLongitud() + " | "+ tempV.darLatitud() + "  --  " + tempW.darId() +" | " + tempW.darLongitud() + " | "+ tempW.darLatitud());
			x = tempV.darId();
		}
		print = false;
		exportarCamino(path, ruta);
		return path;
	}
	public Stack<Edge> darCaminoCostoMinimoDistancia(double pLongitudOrigen, double pLatitudOrigen,double pLongitudDest, double pLatitudDest,String ruta)
	{
		Vertice origen = verticeMasCercano(pLongitudOrigen, pLatitudOrigen);
		System.out.println("Vertice Origen ID: " + origen.darId());
		Vertice dest = verticeMasCercano(pLongitudDest, pLatitudDest);
		System.out.println("Vertice Destino ID: " + dest.darId());
		LinearProbingHashST<Integer,Edge> edgeTo = new LinearProbingHashST<Integer, Edge>();
		LinearProbingHashST<Integer,Double> distTo = new LinearProbingHashST<Integer, Double>();
		IndexHeap<Double> pq = new IndexHeap<Double>(grafo.E());
		Iterator<Vertice> vertices = grafo.getVertex();
		while (vertices.hasNext()) 
		{
			Vertice vertice = (Vertice) vertices.next();
			distTo.put(vertice.darId(), Double.POSITIVE_INFINITY);
		}
		distTo.put(origen.darId(), 0.0);
		pq.insert(origen.darId(), 0.0);

		while (!pq.isEmpty()) 
		{
			//System.out.println("Entre");
			int id = pq.delMin();
			Vertex<Integer,Vertice> v = grafo.getVertex(id);
			ArregloDinamico<Edge> edges = grafo.adjEdges(id);
			for (int i = 0; i < edges.darTamano(); i++) 
			{
				//System.out.println("Entre x2");
				Edge e = edges.darElemento(i);
				Vertex<Integer,Vertice> w = e.other(v);
				if (distTo.get(w.getInfo().darId()) > distTo.get(v.getInfo().darId()) + e.getCost()) 
				{
					distTo.put(w.getInfo().darId(), distTo.get(v.getInfo().darId()) + e.getCost());
					edgeTo.put(w.getInfo().darId(), e);
					if (pq.contains(w.getInfo().darId())) pq.decreaseKey(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
					else                pq.insert(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
				}
			}
		}
		//System.out.println(distTo.get(dest.darId()));
		// Falta construir camino
		Stack<Edge> path = new Stack<Edge>();
		int x = dest.darId();
		System.out.println("Camino (ID | long | lat) -- (ID | long | lat)");
		for (Edge e = edgeTo.get(dest.darId()); e != null; e = edgeTo.get(x)) {
			//System.out.println("Entre x3");
			path.push(e);
			arcos1B++;
			costoDistancia1B+=e.darCosto();
			costoTiempo1B+=e.getTiempo();
			Vertex temp = grafo.getVertex(x);
			Vertice tempW = (Vertice) temp.getInfo();
			Vertice tempV = (Vertice) e.other(temp).getInfo();
			System.out.println(tempV.darId() +" | " + tempV.darLongitud() + " | "+ tempV.darLatitud() + "  --  " + tempW.darId() +" | " + tempW.darLongitud() + " | "+ tempW.darLatitud());
			x = tempV.darId();
		}
		print = false;
		exportarCamino(path, ruta);
		print = true;
		return path;
	}
	//	
	//	public ArregloDinamico<Integer> verticesAlcanzablesEnTiempo(double Longitud,double Latitud,double tiempo)
	//	{
	//		ArregloDinamico<Integer> verticesAlcanzables=new ArregloDinamico<Integer>();
	//		LinearProbingHashST<Integer,Double> distTo = new LinearProbingHashST<Integer, Double>();
	//		Vertice origen = verticeMasCercano(Longitud, Latitud);
	//		int idOrigen=origen.darId();
	//		ArregloDinamico<Edge> adj = grafo.adjEdges(idOrigen);
	//		for (int i=0;i<adj.darTamano();i++)
	//		{
	//			Edge e=adj.darElemento(i);
	//			if (e.getTiempo()<=tiempo)
	//			{
	//				Vertice v=(Vertice)e.other(grafo.getVertex(idOrigen)).getInfo();
	//			}
	//		}
	//		for (int i=0;i<arcos.darTamano();i++)
	//		{
	//			Edge e=adj.darElemento(i);
	//			Vertex<Integer,Vertice> w = e.other(v);
	//	        if (distTo.get(w.getInfo().darId()) > distTo.get(v.getInfo().darId()) + e.getCost()) 
	//	        {
	//	            distTo.put(w.getInfo().darId(), distTo.get(v.getInfo().darId()) + e.getCost());
	//	            edgeTo.put(w.getInfo().darId(), e);
	//	            if (pq.contains(w.getInfo().darId())) pq.decreaseKey(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
	//	            else                pq.insert(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
	//	        }
	//		}
	//
	//		
	//		return verticesAlcanzables;
	//		
	//	}
	//	
	//	public ArregloDinamico<Edge> verticesRecursivo(ArregloDinamico<Edge> adj, int id, double tiempo)
	//	{
	//		ArregloDinamico<Edge> arcos=new ArregloDinamico<Edge>();
	//		LinearProbingHashST<Integer,Double> timeTo = new LinearProbingHashST<Integer, Double>();
	//		for (int i=0;i<arcos.darTamano();i++)
	//		{
	//			Edge e=adj.darElemento(i);
	//			Vertice vI=(Vertice)grafo.getVertex(id).getInfo();
	//			Vertice vF=(Vertice)e.other(grafo.getVertex(id)).getInfo();
	//			if (timeTo.get(vI.darId())+e.getTiempo()<=tiempo)
	//			{
	//				timeTo.putInSet(vF.darId(), timeTo.get(vI.darId())+e.getTiempo());		
	//			}
	//			
	//		}
	//		return arcos;
	//	}
	public Edge verticeToEdge(Vertex v)
	{
		return new Edge(v,v, 0 , 0 ,0 );
	}

	public Stack<Edge> verticesMenorVelocidadPromedio(int n, String ruta)
	{
		MaxHeapCP<PairDouble> velocidades = new MaxHeapCP<PairDouble>();
		Graph<Integer,Vertice> grafoVelocidades = new Graph<Integer, Vertice>();
		Iterator<Vertex> vertices = grafo.getVertexClean();
		while (vertices.hasNext()) 
		{
			Vertex v = (Vertex) vertices.next();
			Vertice vInfo = (Vertice) v.getInfo();
			ArregloDinamico<Edge> edges = v.getAdjacents();
			double velocidadSuma = 0;
			double velProm = 0;
			for (int i = 0; i < edges.darTamano(); i++) {
				Edge e = edges.darElemento(i);
				velocidadSuma+=e.getVelocidad();
			}
			if (edges.darTamano()>0) {
				velProm = velocidadSuma/edges.darTamano();
			}

			PairDouble vert = new PairDouble(vInfo.darId(), velProm);
			velocidades.insertar(vert);
		}
		// A�adir vertices
		System.out.println("Vertices:");
		System.out.println("-");
		for (int i = 0; i < n && !velocidades.esVacia(); i++) 
		{
			PairDouble pTemp = velocidades.sacarMax();
			Vertice vTemp = grafo.getInfoVertex(pTemp.getKey());
			System.out.println("Vertice ID: " + vTemp.darId());
			System.out.println("Vertice velocidad: " + pTemp.getValue());
			System.out.println("Ubicacion(long | lat): " + vTemp.darLongitud()+ " | " + vTemp.darLatitud());
			System.out.println("-");
			
			grafoVelocidades.addVertex(pTemp.getKey(), vTemp);
		}
		// A�adir arcos
		Iterator<Vertex> verticesSimp = grafoVelocidades.getVertexClean();
		while (verticesSimp.hasNext()) {
			Vertex vertex = (Vertex) verticesSimp.next();
			Vertice vInfo = (Vertice) vertex.getInfo();
			ArregloDinamico<Edge> edgesSimp = grafo.adjEdges(vInfo.darId());
			for (int i = 0; i < edgesSimp.darTamano(); i++) 
			{
				Edge eTempSimp = edgesSimp.darElemento(i);
				Vertice v1 = (Vertice) eTempSimp.either().getInfo();
				Vertice v2 = (Vertice) eTempSimp.other(eTempSimp.either()).getInfo();
				grafoVelocidades.addEdge(v1.darId(), v2.darId(), eTempSimp.darCosto(), eTempSimp.getTiempo(), eTempSimp.getVelocidad());
			}
		}
		// Ver CC mas grande
		grafoVelocidades.uncheck();
		System.out.println("El n�mero de cc es: " + grafoVelocidades.cc());
		MaxHeapCP<Pair> cola=new MaxHeapCP<Pair>();
		Iterator<Integer> iter=grafoVelocidades.numCC();
		LinearProbingHashST<Integer,Integer> st=new LinearProbingHashST<Integer,Integer>();
		int numrep=0;
		while (iter.hasNext())
		{
			int codigoCC=iter.next();
			if (st.get(codigoCC)!=null)
			{
				st.put(codigoCC, st.get(codigoCC)+1);
			}
			else
			{
				st.put(codigoCC, 1);
			}
		}
		Iterator<Integer> iter1=st.keys();
		while (iter1.hasNext())
		{
			int cc = iter1.next();
			Pair temp = new Pair(cc, st.get(cc));
			cola.insertar(temp);
		}
		int codigoCC = cola.darMax().getKey();
		// Crear Edges
		Stack<Edge> grafoVel = new Stack<Edge>();
		verticesSimp = grafoVelocidades.getVertexClean();
		while (verticesSimp.hasNext()) {
			Vertex vertex = (Vertex) verticesSimp.next();
			Vertice vInfo = (Vertice) vertex.getInfo();
			ArregloDinamico<Edge> edgesSimp = grafoVelocidades.adjEdges(vInfo.darId());
			if (vertex.getCC()==codigoCC) 
			{
				for (int i = 0; i < edgesSimp.darTamano(); i++) 
				{
					grafoVel.push(edgesSimp.darElemento(i));
				}
			}
			if (edgesSimp.darTamano()==0) grafoVel.push(verticeToEdge(vertex));
		}
		print= false;
		exportarCamino(grafoVel, ruta);
		print=true;
		return grafoVel;
	}

	public int ccMasGrande()
	{
		grafo.uncheck();
		grafo.cc();
		MaxHeapCP<Pair> cola=new MaxHeapCP<Pair>();
		Iterator<Integer> iter=grafo.numCC();
		LinearProbingHashST<Integer,Integer> st=new LinearProbingHashST<Integer,Integer>();
		int numrep=0;
		while (iter.hasNext())
		{
			int codigoCC=iter.next();
			if (st.get(codigoCC)!=null)
			{
				st.put(codigoCC, st.get(codigoCC)+1);
			}
			else
			{
				st.put(codigoCC, 1);
			}
		}
		Iterator<Integer> iter1=st.keys();
		while (iter1.hasNext())
		{
			int cc = iter1.next();
			Pair temp = new Pair(cc, st.get(cc));
			cola.insertar(temp);
		}
		return cola.darMax().getKey();
	}

	public Stack<Edge> mstPrim(String ruta)
	{ 
		Vertice origen = (Vertice) grafo.getVerticeCC(ccMasGrande());
		grafo.uncheck();
		System.out.println(origen.darId());
		LinearProbingHashST<Integer,Edge> edgeTo = new LinearProbingHashST<Integer, Edge>();
		LinearProbingHashST<Integer,Double> distTo = new LinearProbingHashST<Integer, Double>();
		IndexHeap<Double> pq = new IndexHeap<Double>(grafo.E());
		Iterator<Vertice> vertices = grafo.getVertex();
		while (vertices.hasNext()) 
		{
			Vertice vertice = (Vertice) vertices.next();
			distTo.put(vertice.darId(), Double.POSITIVE_INFINITY);
		}
		distTo.put(origen.darId(), 0.0);
		pq.insert(origen.darId(), 0.0);

		while (!pq.isEmpty()) 
		{
			//System.out.println("Entre");
			int id = pq.delMin();
			Vertex<Integer,Vertice> v = grafo.getVertex(id);
			v.mark();
			ArregloDinamico<Edge> edges = grafo.adjEdges(id);
			for (int i = 0; i < edges.darTamano(); i++) 
			{
				//System.out.println("Entre x2");
				Edge e = edges.darElemento(i);
				Vertex<Integer,Vertice> w = e.other(v);
				if(!w.isMarked()) {
					if (e.getCost()<distTo.get(w.getInfo().darId())) 
					{
						distTo.put(w.getInfo().darId(),  e.getCost());
						edgeTo.put(w.getInfo().darId(), e);
						if (pq.contains(w.getInfo().darId())) pq.decreaseKey(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
						else                pq.insert(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
					}
				}
			}
		}
		//System.out.println(distTo.get(dest.darId()));
		// Falta construir camino
		Stack<Edge> tree = new Stack<Edge>();
		Iterator<Integer> edges = edgeTo.keys();
		while (edges.hasNext()) 
		{
			//System.out.println("Entre x3");
			Integer idTemp = (Integer) edges.next();
			Edge eTemp = edgeTo.get(idTemp);
			if(eTemp != null) 
			{
				costoTotalPrim+=eTemp.darCosto();
				tree.push(eTemp);
			}
		}
		contadorPrim = tree.getSize();
		exportarCamino(tree, ruta);
		return tree;
	}

	public LinearProbingHashST<Integer,Double> verticesAlcanzablesEnTiempo(double Longitud,double Latitud,double tiempo,String ruta)
	{
		LinearProbingHashST<Integer,Double> timeTo = new LinearProbingHashST<Integer, Double>();
		Vertice origen = verticeMasCercano(Longitud, Latitud);
		int idOrigen=origen.darId();
		System.out.println("Id origen: "+idOrigen);
		ArregloDinamico<Edge> adj = grafo.adjEdges(idOrigen);
		timeTo.put(idOrigen, 0.0);
		verticesRecursivo(adj, idOrigen, tiempo, timeTo);
		exportarHash(timeTo,ruta);
		return timeTo;

	}

	public LinearProbingHashST<Integer,Double> verticesRecursivo(ArregloDinamico<Edge> adj, int id, double tiempo, LinearProbingHashST<Integer, Double>timeTo)
	{
		for (int i=0;i<adj.darTamano();i++)
		{
			Edge e=adj.darElemento(i);
			Vertice vI=(Vertice)grafo.getVertex(id).getInfo();
			Vertice vF=(Vertice)e.other(grafo.getVertex(id)).getInfo();
			if (timeTo.getSet(vF.darId())==null)
			{
				if (timeTo.get(vI.darId())+e.getTiempo()<=tiempo)
				{
					timeTo.put(vF.darId(), timeTo.get(vI.darId())+e.getTiempo());	
					verticesRecursivo(grafo.adjEdges(vF.darId()), vF.darId(), tiempo,timeTo);
				}
			}

		}
		return timeTo;
	}


	public Stack<Edge> kruskal(String ruta)
	{

		Graph<Integer, Vertice> grafito=new Graph<Integer, Vertice>();
		Stack<Edge> mst=new Stack<Edge>();
		MinHeap<Edge> pq=new MinHeap<Edge>();
		int codigocc=ccMasGrande();
		Vertice v= grafo.getVerticeCC(codigocc);
		Iterator <Integer> iter=grafo.getCC(v.darId());
		crearGrafoCCMasGrande(grafito, iter);
		ArregloDinamico<Edge> edges=grafito.getEdges();
		for (int i =0;i<grafito.E();i++)
		{
			Edge e=edges.darElemento(i);
			pq.insertar(e);
		}
		UnionFind uf=new UnionFind(grafito.E());
		while (!pq.esVacia() && mst.getSize()<grafito.V()-1)
		{
			Edge e=pq.sacarMax();
			Vertex vx=e.either();
			Vertice v1=(Vertice)e.either().getInfo();
			int idv1=v1.darId();
			Vertex wx=e.other(vx);
			Vertice w=(Vertice)wx.getInfo();
			int idw=w.darId();
			if (uf.connected(idv1, idw))
			{
				continue;
			}
			uf.union(idv1, idw);
			mst.push(e);
			contadorKruskal++;
			costoTotalKruskal+=e.getCost();
		}
		exportarCamino(mst, ruta);
		return mst;

	}
	public void crearGrafoCCMasGrande(Graph<Integer, Vertice> grafito, Iterator <Integer> iter)
	{
		ArregloDinamico<Edge> edges=new ArregloDinamico<Edge>(10000);
		while (iter.hasNext())
		{
			int id = iter.next();
			if (grafito.getVertex(id)==null)
			{
				grafito.addVertex(id, (Vertice)grafo.getVertex(id).getInfo());
			}
		}
		Iterator<Vertice> iterator=grafito.getVertex();
		while (iterator.hasNext())
		{
			Vertice v=iterator.next();

			int id=v.darId();
			edges=grafo.adjEdges(id);
			for (int i=0;i<edges.darTamano();i++)
			{
				Vertice otro=(Vertice)edges.darElemento(i).other(grafo.getVertex(id)).getInfo();
				grafito.addEdge(id,otro.darId() , edges.darElemento(i).darCosto(), edges.darElemento(i).getTiempo(), edges.darElemento(i).getVelocidad());
			}
		}
	}
	
	public Vertice verticeMasCercanoZona(double pLongitud, double pLatitud)
	{
		Iterator<Vertice> vertices = grafoZonas.getVertex();
		Vertice vertex = null;
		double min = Double.POSITIVE_INFINITY;
		while (vertices.hasNext()) 
		{
			Vertice vertice = (Vertice) vertices.next();
			double dist = Haversine.distance(pLatitud, pLongitud, vertice.darLatitud(), vertice.darLongitud());
			if (dist<min) {
				min = dist;
				vertex = vertice;
			}
		}
		return vertex;
	}
	
	public Stack<Edge> darCaminoCostoMinimoDistanciaZonas(double pLongitudOrigen, double pLatitudOrigen,double pLongitudDest, double pLatitudDest,String ruta)
	{
		Vertice origen = verticeMasCercanoZona(pLongitudOrigen, pLatitudOrigen);
		System.out.println("Vertice Origen ID: " + origen.darId());
		Vertice dest = verticeMasCercanoZona(pLongitudDest, pLatitudDest);
		System.out.println("Vertice Destino ID: " + dest.darId());
		LinearProbingHashST<Integer,Edge> edgeTo = new LinearProbingHashST<Integer, Edge>();
		LinearProbingHashST<Integer,Double> distTo = new LinearProbingHashST<Integer, Double>();
		IndexHeap<Double> pq = new IndexHeap<Double>(grafo.E());
		Iterator<Vertice> vertices = grafoZonas.getVertex();
		while (vertices.hasNext()) 
		{
			Vertice vertice = (Vertice) vertices.next();
			distTo.put(vertice.darId(), Double.POSITIVE_INFINITY);
		}
		distTo.put(origen.darId(), 0.0);
		pq.insert(origen.darId(), 0.0);

		while (!pq.isEmpty()) 
		{
			//System.out.println("Entre");
			int id = pq.delMin();
			Vertex<Integer,Vertice> v = grafoZonas.getVertex(id);
			ArregloDinamico<Edge> edges = grafoZonas.adjEdges(id);
			for (int i = 0; i < edges.darTamano(); i++) 
			{
				//System.out.println("Entre x2");
				Edge e = edges.darElemento(i);
				Vertex<Integer,Vertice> w = e.other(v);
				if (distTo.get(w.getInfo().darId()) > distTo.get(v.getInfo().darId()) + e.getTiempo()) 
				{
					distTo.put(w.getInfo().darId(), distTo.get(v.getInfo().darId()) + e.getTiempo());
					edgeTo.put(w.getInfo().darId(), e);
					if (pq.contains(w.getInfo().darId())) pq.decreaseKey(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
					else                pq.insert(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
				}
			}
		}
		//System.out.println(distTo.get(dest.darId()));
		// Falta construir camino
		Stack<Edge> path = new Stack<Edge>();
		int x = dest.darId();
		System.out.println("Camino (ID | long | lat) -- (ID | long | lat)");
		for (Edge e = edgeTo.get(dest.darId()); e != null; e = edgeTo.get(x)) {
			//System.out.println("Entre x3");
			path.push(e);
			arcos2C++;
			//costoDistancia1B+=e.darCosto();
			costoTiempo2C+=e.getTiempo();
			Vertex temp = grafoZonas.getVertex(x);
			Vertice tempW = (Vertice) temp.getInfo();
			Vertice tempV = (Vertice) e.other(temp).getInfo();
			//System.out.println(tempV.darId() +" | " + tempV.darLongitud() + " | "+ tempV.darLatitud() + "  --  " + tempW.darId() +" | " + tempW.darLongitud() + " | "+ tempW.darLatitud());
			x = tempV.darId();
		}
		exportarCamino(path, ruta);
		return path;
	}
	public Stack<Edge> darCaminoMasVerticesZonas(double pLongitudOrigen, double pLatitudOrigen,String ruta)
	{
		Vertice origen = verticeMasCercanoZona(pLongitudOrigen, pLatitudOrigen);
		System.out.println("Vertice Origen ID: " + origen.darId());
		LinearProbingHashST<Integer,Edge> edgeTo = new LinearProbingHashST<Integer, Edge>();
		LinearProbingHashST<Integer,Integer> distTo = new LinearProbingHashST<Integer, Integer>();
		IndexHeap<Integer> pq = new IndexHeap<Integer>(grafoZonas.E());
		Iterator<Vertice> vertices = grafoZonas.getVertex();
		while (vertices.hasNext()) 
		{
			Vertice vertice = (Vertice) vertices.next();
			distTo.put(vertice.darId(), Integer.MAX_VALUE);
		}
		distTo.put(origen.darId(), 0);
		pq.insert(origen.darId(), 0);

		while (!pq.isEmpty()) 
		{
			//System.out.println("Entre");
			int id = pq.delMin();
			Vertex<Integer,Vertice> v = grafoZonas.getVertex(id);
			ArregloDinamico<Edge> edges = grafoZonas.adjEdges(id);
			for (int i = 0; i < edges.darTamano(); i++) 
			{
				//System.out.println("Entre x2");
				Edge e = edges.darElemento(i);
				Vertex<Integer,Vertice> w = e.other(v);
				if (distTo.get(w.getInfo().darId()) > distTo.get(v.getInfo().darId()) + 1) 
				{
					distTo.put(w.getInfo().darId(), distTo.get(v.getInfo().darId()) + 1);
					edgeTo.put(w.getInfo().darId(), e);
					if (pq.contains(w.getInfo().darId())) pq.decreaseKey(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
					else                pq.insert(w.getInfo().darId(), distTo.get(w.getInfo().darId()));
				}
			}
		}
		//System.out.println(distTo.get(dest.darId()));
		// Falta construir camino
		Stack<Edge> path = new Stack<Edge>();
		Iterator<Integer> keys = distTo.keys();
		int max = 0;
		Vertice dest = origen;
		while (keys.hasNext()) 
		{
			Integer integer = (Integer) keys.next();
			if(max<distTo.get(integer))
			{
				max = distTo.get(integer);
				dest = grafoZonas.getInfoVertex(integer);
			}
		}
		
		int x = dest.darId();
		System.out.println("Camino (ID | long | lat) -- (ID | long | lat)");
		for (Edge e = edgeTo.get(dest.darId()); e != null; e = edgeTo.get(x)) {
			//System.out.println("Entre x3");
			path.push(e);
			//arcosB++;
			//costoDistancia1B+=e.darCosto();
			//costoTiempo2C+=e.getTiempo();
			Vertex temp = grafoZonas.getVertex(x);
			Vertice tempW = (Vertice) temp.getInfo();
			Vertice tempV = (Vertice) e.other(temp).getInfo();
			//System.out.println(tempV.darId() +" | " + tempV.darLongitud() + " | "+ tempV.darLatitud() + "  --  " + tempW.darId() +" | " + tempW.darLongitud() + " | "+ tempW.darLatitud());
			x = tempV.darId();
		}
		exportarCamino(path, ruta);
		return path;
	}
	
	public Graph<Integer, Vertice> grafoZonas1()
	{
		Graph<Integer, Vertice> g=new Graph<Integer, Vertice>();

		LinearProbingHashST<Integer, Integer>verticeshash=new LinearProbingHashST<Integer,Integer>();
        LinearProbingHashST<String, Integer>arcohash=new LinearProbingHashST<String, Integer>();
        ArregloDinamico<Edge> edges=grafo.getEdges();
        boolean vexiste=false;
        boolean wexiste=false;
        boolean arcoexiste=false;
        double costo=0.0;

        for (int i = 0; i < edges.darTamano(); i++) 
        {
        	Edge e=edges.darElemento(i);
        	Vertex vx=e.either();
			Vertice v=(Vertice) vx.getInfo();
			Vertice w=(Vertice) e.other(vx).getInfo();
            int idv = v.darZona();
            int idw = w.darZona();
            if (idv != idw) 
            {
               vexiste = verticeshash.contains(idv);
               wexiste= verticeshash.contains(idw);
               arcoexiste = arcohash.contains(idv+"-"+idw) && arcohash.contains(idw+"-"+idv);
               if (!wexiste) 
               {
                   verticeshash.put(idw, 0);
                   w.setId(idw);
                   g.addVertex(idw, w);
               }
                if (!vexiste) 
                {
                    verticeshash.put(idv, 0);
                    v.setId(idv);
                    g.addVertex(idv, v);
                }
                if (!arcoexiste) 
                {
                    costo = (PromedioCostos(v, w) + PromedioCostos(w, v)) / 2;
                    if (costo==100.0) 
                    {
                        costo=200.0;
                    }
                    g.addEdge(idv, idw, 0, costo, 0);
                    arcohash.put(idv+"-"+idw, 0);
                    arcohash.put(idw+"-"+idv, 0);
                }
            }
        }
        grafoZonas = g;
        return g;
	}
	
    private Double PromedioCostos(Vertice pv, Vertice pw)
    {

    	LinearProbingHashST<Integer, Double> hash=new LinearProbingHashST<Integer, Double>();
    	LinearProbingHashST<Integer, Double> contador=new LinearProbingHashST<Integer, Double>();
    	double tiempoProm=0.0; 
        
    	for (int i=0;i<grafo.getEdges().darTamano();i++)
    	{
    		Edge e=grafo.getEdges().darElemento(i);
    		Vertex vx=e.either();
			Vertice v=(Vertice) vx.getInfo();
			Vertice w=(Vertice) e.other(vx).getInfo();
			
			if (hash.get(v.darZona())!=null)
			{
				double act=hash.get(v.darZona());
				double cont=contador.get(v.darZona());
				hash.put(v.darZona(), act+e.getTiempo());
				contador.put(v.darZona(), cont++);
			}
			else
			{
				hash.put(v.darZona(), e.getTiempo());
				contador.put(v.darZona(), 1.0);
			}
			if (hash.get(w.darZona())!=null)
			{
				double act=hash.get(w.darZona());
				double cont=contador.get(w.darZona());
				hash.put(v.darZona(), act+e.getTiempo());
				contador.put(w.darZona(), cont++);
			}
			else
			{
				hash.put(w.darZona(), e.getTiempo());
				contador.put(w.darZona(), 1.0);
			}
    	}
    	for (int i=0;i<grafo.getEdges().darTamano();i++)
    	{
    		Edge e=grafo.getEdges().darElemento(i);
    		Vertex vx=e.either();
			Vertice v=(Vertice) vx.getInfo();
			Vertice w=(Vertice) e.other(vx).getInfo();
    	}
    
        return tiempoProm;
    }
	public void reiniciar()
	{
		arcos1A = 0;
		costoTiempo1A = 0;
		costoDistancia1A = 0;
		arcos1B = 0;
		costoTiempo1B = 0;
		costoDistancia1B = 0;
	}
	public int darArcos3C() {
		// TODO Auto-generated method stub
		return arcos3C;
	}
}

package model.logic;

public class Pair implements Comparable<Pair> {
	
	private int key;
	private int value;
	
	public Pair(int k, int v)
	{
		key = k;
		value = v;
	}
	
	public int getKey()
	{
		return key;
	}
	
	public int getValue()
	{
		return value;
	}

	@Override
	public int compareTo(Pair o) {
		// TODO Auto-generated method stub
		if(value>o.value) return 1;
		else if(value<o.value) return -1;
		else return 0;
	}
	
	
}

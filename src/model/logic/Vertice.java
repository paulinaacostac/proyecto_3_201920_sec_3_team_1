package model.logic;

public class Vertice implements Comparable<Vertice>
{
	private int id;
	private double longitud;
	private double latitud;
	private int zona;
	
	public Vertice (int pId, double pLongitud, double pLatitud, int pZona)
	{
		id=pId;
		longitud=pLongitud;
		latitud=pLatitud;
		zona=pZona;
	}
	
	public int darId()
	{
		return id;
	}
	public double darLongitud()
	{
		return longitud;
	}
	public double darLatitud()
	{
		return latitud;
	}
	public int darZona()
	{
		return zona;
	}
	public String toString()
	{
		return "ID: "+id+" Longitud: "+longitud+" Latitud: "+latitud+" Zona: "+zona;
	}
	@Override
	public int compareTo(Vertice v) 
	{
		
		return 0;
	}

	public void setId(int idw) {
		// TODO Auto-generated method stub
		id = idw;
	}
}

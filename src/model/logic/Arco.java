package model.logic;

import model.data_structures.ArregloDinamico;

public class Arco implements Comparable<Arco>
{
	private ArregloDinamico<Integer> verticesAdyacentes;
	private double distancia;
	private double tiempo;
	private double velocidad;
	
	public Arco (ArregloDinamico<Integer> vertices, double pDistancia, double pTiempo, double pVelocidad)
	{
		verticesAdyacentes=vertices;
		distancia=pDistancia;
		tiempo=pTiempo;
		velocidad=pVelocidad;
	}
	public ArregloDinamico<Integer> darVerticesAdyacentes()
	{
		return verticesAdyacentes;
	}
	public double darDistancia()
	{
		return distancia;
	}
	public double darTiempo()
	{
		return tiempo;
	}
	public double darVelocidad()
	{
		return velocidad;
	}
	public void cambiarTiempoyVel(double t)
	{
		tiempo=t;
		velocidad=distancia/t;
	}
	public String toString()
	{
		String r="";
		for (int i=0;i<verticesAdyacentes.darTamano();i++)
		{
			r+="Vertice "+i+" : "+verticesAdyacentes.darElemento(i)+"\n";
		}
		r+="Distancia: "+distancia;
		return r;
	}
	@Override
	public int compareTo(Arco o) {
		// TODO Auto-generated method stub
		return 0;
	}

}

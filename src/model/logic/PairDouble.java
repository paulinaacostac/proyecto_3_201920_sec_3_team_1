package model.logic;

public class PairDouble implements Comparable<PairDouble>{
	private int key;
	private double value;
	
	public PairDouble(int k, double v)
	{
		key = k;
		value = v;
	}
	
	public int getKey()
	{
		return key;
	}
	
	public double getValue()
	{
		return value;
	}

	@Override
	public int compareTo(PairDouble o) {
		// TODO Auto-generated method stub
		if(value<o.value) return 1;
		else if(value>o.value) return -1;
		else return 0;
	}
	
	
}

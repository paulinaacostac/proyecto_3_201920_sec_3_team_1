package model.logic;

import com.google.gson.annotations.SerializedName;

public class Properties 
{
	@SerializedName("distancia|id")
	private double distanciaoId;
	
	@SerializedName("dv1|zona")
	private int idv1oZona;
	
	private int idv2;
	
	private double tiempo;
	
	private double velocidad;
	
	public double darDistanciaoId() 
	{
		return distanciaoId;
	}
	
	public int daridV1oZona() 
	{
		return idv1oZona;
	}
	
	public int daridV2() 
	{
		return idv2;
	}
	public double darTiempo() 
	{
		return tiempo;
	}
	public double darVelocidad() 
	{
		return velocidad;
	}
	

}

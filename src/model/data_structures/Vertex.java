package model.data_structures;

import java.util.Iterator;


public class Vertex<K extends Comparable<K>, V>
{

	private K key;
	private V info;
	private boolean mark;
	private ArregloDinamico<Edge> adjacents;
	private int cc;

	public Vertex(K key, V infoVertex) {
		this.mark = false;
		this.key = key;
		this.setInfo(infoVertex);
		adjacents = new ArregloDinamico<Edge>();
		this.cc = -1;
	}
	
	public V getInfo() {
		return info;
	}

	public ArregloDinamico<Edge> getAdjacents() {
		return adjacents;
	}

	public void setInfo(V info) {
		this.info = info;
	}

	public void mark() {
		mark = true;
	}

	public void setCC(int cc) {
		this.cc = cc;
	}
	
	public int getCC()
	{
		return cc;
	}

	public void unmark() {
		cc = -1;
		mark = false;
	}

	public boolean isMarked()
	{
		return mark;
	}

	public boolean equals(Vertex<K,V> o) 
	{
		return key.equals(o.key);
	}

	public void addEdge(Edge e){
		adjacents.agregar(e);
	}

	public Iterator<K> adj()
	{
		Queue<K> iter = new Queue<K>();
		for (int i = 0; i < getAdjacents().darTamano(); i++) 
		{
			K temp = (K) getAdjacents().darElemento(i).other(this).key;
			iter.enqueue(temp);
		}
		return iter;
	}


	public void dfs()
	{
		mark();
		for (int i = 0; i < getAdjacents().darTamano(); i++) 
		{
			Edge e = getAdjacents().darElemento(i);
			Vertex<K,V> other = e.other(this);
			if (!other.isMarked()) other.dfs();
		}
	}

	public void markCC(int cc)
	{
		mark();
		setCC(cc);
		for (int i = 0; i < adjacents.darTamano(); i++) 
		{
			Edge e = adjacents.darElemento(i);
			Vertex<K,V> other = e.other(this);
			if (!other.isMarked()) other.markCC(cc);
		}
	}
}

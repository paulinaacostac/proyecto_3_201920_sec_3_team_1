package model.data_structures;


public class Edge implements Comparable<Edge>{

	private Vertex v;
	private Vertex w;
	private double cost;
	private double tiempo;
	private double velocidad;


	public Edge(Vertex v, Vertex w, double cost, double tiempo, double velocidad) 
	{
		this.v = v;
		this.w = w;
		this.setCost(cost);
		this.setTiempo(tiempo);
		this.setVelocidad(velocidad);
	}
	
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getTiempo() {
		return tiempo;
	}

	public void setTiempo(double tiempo) {
		this.tiempo = tiempo;
	}

	public double getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}

	public double darCosto()
	{
		return getCost();
	}

	public Vertex either()
	{
		return v;
	}

	public Vertex other(Vertex v) 
	{
		if (v.equals(this.v)) return w;
		else return this.v;
	}

//	public V otherId(V v) 
//	{
//		if (v.equals(this.v)) return w.info;
//		else return this.v.info;
//	}

	public int compareTo(Edge o){
		if		(this.getCost() > o.getCost()) return +1;
		else if (this.getCost() < o.getCost()) return -1;
		else return 0;
	}

}

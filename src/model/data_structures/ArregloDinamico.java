package model.data_structures;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico<T extends Comparable<T>> implements IArregloDinamico<T>
{
	/**
	 * Capacidad maxima del arreglo
	 */
	private int tamanoMax;
	/**
	 * Numero de elementos presentes en el arreglo (de forma compacta desde la posicion 0)
	 */
	private int tamanoAct;
	/**
	 * Arreglo de elementos de tamaNo maximo
	 */
	private T elementos[ ];

	/**
	 * Construir un arreglo con la capacidad no especificada.
	 */
	public ArregloDinamico()
	{
		int max = 100;
		elementos = (T[]) new Comparable[max];
		tamanoMax = max;
		tamanoAct = 0;
	}
	
	/**
	 * Construir un arreglo con la capacidad maxima inicial.
	 * @param max Capacidad maxima inicial
	 */
	public ArregloDinamico(Integer max)
	{
		elementos = (T[]) new Comparable[max];
		tamanoMax = max;
		tamanoAct = 0;
	}

	public void agregar( T dato )
	{
		if ( tamanoAct == tamanoMax )
		{  // caso de arreglo lleno (aumentar tamaNo)
			tamanoMax = 2 * tamanoMax;
			T [ ] copia = elementos;
			elementos = (T[]) new Comparable[tamanoMax];
			for ( int i = 0; i < tamanoAct; i++)
			{
				elementos[i] = copia[i];
			} 
			//			System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
		}	
		elementos[tamanoAct] = dato;
		tamanoAct++;
	}

	public int darCapacidad() {
		return tamanoMax;
	}

	public int darTamano() {
		return tamanoAct;
	}

	public T darElemento(int i) {
		// TODO implementar
		return elementos[i];
	}

	public T buscar(T dato) {
		// TODO implementar
		// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
		T t=null;
		for (int i = 0; i < elementos.length; i++) 
		{
			if (elementos[i]==null)
			{
				t=null;
			}
			else
			{
				if(elementos[i].compareTo(dato)==0)
				{
					t=elementos[i];
				}
			}
			
		}
		return t;
	}
	public void set(T dato, int i)
	{
		elementos[i] = dato;
	}
	public void exch(int dato, int dato2)
	{
		T obj=elementos[dato];
		elementos[dato]=elementos[dato2];
		elementos[dato2]=obj;
	}
	public void shuffle()
	{
		int N = elementos.length;
		for (int i = 0; i < N; i++)
		{ 
			int r = i + (int)(Math.random()*(N-i));
			System.out.println(r);
			exch(i,r);
		}
	}

	public T eliminar(T dato) {
		// TODO implementar
		// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
		boolean encontrado = false;
		T eliminado = null;
		for (int i = 0; i < elementos.length-1 && !encontrado; i++) 
		{
			T t = elementos[i];
			if (dato==null)
			{
				System.out.println("dato parametro null en pos: "+i);
			}
			if (t==null)
			{
				System.out.println("dato t null en pos: "+i);
			}

			if (t.compareTo(dato)==0) 
			{
				eliminado = t;
				for (int j = i; j < elementos.length-1; j++) 
				{
					elementos[j] = elementos[j+1];
				}
				tamanoAct--;
			}
		}
		if (eliminado==null)
		{
			System.out.println("No se encontro elemento a elminar");
		}
		return eliminado;
	}

}


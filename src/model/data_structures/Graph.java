/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author Team 1
 *
 */
public class Graph<K extends Comparable<K>, V> {

	private int V;
	private int E;
	private LinearProbingHashST<K, Vertex<K,V>> adj;
	private ArregloDinamico<Edge> edges;


	public Graph() {
		adj = new LinearProbingHashST<K,Vertex<K,V>>();
		E = 0;
		V = 0;
		edges = new ArregloDinamico<Edge>();
	}

	public int V() {
		return V;
	}

	public int E() {
		return E;
	}
	
	public Iterator<V> getVertex()
	{ 
		Queue<V> vertex = new Queue<V>();
		Iterator<K> keys = adj.keys();
		while (keys.hasNext()) 
		{
			K k = (K) keys.next();
			vertex.enqueue(adj.get(k).getInfo());
		}
		return vertex;
	}
	
	public Iterator<Vertex> getVertexClean()
	{ 
		Queue<Vertex> vertex = new Queue<Vertex>();
		Iterator<K> keys = adj.keys();
		while (keys.hasNext()) 
		{
			K k = (K) keys.next();
			vertex.enqueue(adj.get(k));
		}
		return vertex;
	}
	
	public ArregloDinamico<Edge> getEdges()
	{
		return edges;
	}

	public void addEdge(K idVertexIni, K idVertexFin, double cost, double tiempo, double velocidad) 
	{
		if (adj.contains(idVertexIni) && adj.contains(idVertexFin)) {
			Vertex v = adj.get(idVertexIni);
			Vertex w = adj.get(idVertexFin);
			Edge e = new Edge(v, w, cost,tiempo,velocidad);
			v.addEdge(e);
			w.addEdge(e);
			edges.agregar(e);
			E++;
		}
	}
	
	public Vertex getVertex(K idVertex) 
	{
		if (adj.get(idVertex)!=null)
		{
			return adj.get(idVertex);
		}
		return null;

	}
	
	public V getInfoVertex(K idVertex) 
	{
		if (adj.get(idVertex)!=null)
		{
			return adj.get(idVertex).getInfo();
		}
		return null;

	}

	public void setInfoVertex(K idVertex, V infoVertex) 
	{
		adj.get(idVertex).setInfo(infoVertex);
	}

	public Edge getArc(K idVertexIni, K idVertexFin)
	{
		Vertex v = adj.get(idVertexIni);
		Vertex w = adj.get(idVertexFin);
		ArregloDinamico<Edge> edges =adj.get(idVertexIni).getAdjacents();
		for (int i = 0; i < edges.darTamano(); i++) 
		{
			Edge e = edges.darElemento(i);
			Vertex temp = e.either();
			if (temp.equals(v) & e.other(temp).equals(w)) return e;
			else if (temp.equals(w) & e.other(temp).equals(v)) return e;
		}
		return null;
	}
	public double getCostArc(K idVertexIni, K idVertexFin) 
	{
		Vertex v = adj.get(idVertexIni);
		Vertex w = adj.get(idVertexFin);
		ArregloDinamico<Edge> edges =adj.get(idVertexIni).getAdjacents();
		for (int i = 0; i < edges.darTamano(); i++) 
		{
			Edge e = edges.darElemento(i);
			Vertex temp = e.either();
			if (temp.equals(v) & e.other(temp).equals(w)) return e.getCost();
			else if (temp.equals(w) & e.other(temp).equals(v)) return e.getCost();
		}
		return -1;
	}

	public void setCostArc(K idVertexIni, K idVertexFin, double tiempo) {
		Vertex v = adj.get(idVertexIni);
		Vertex w = adj.get(idVertexFin);
		ArregloDinamico<Edge> edges =adj.get(idVertexIni).getAdjacents();
		for (int i = 0; i < edges.darTamano(); i++) 
		{
			Edge e = edges.darElemento(i);
			Vertex temp = e.either();
			if (temp.equals(v) & e.other(temp).equals(w)) {
				e.setTiempo(tiempo);
				e.setVelocidad(e.getCost()/tiempo);
				return;
			}
			else if (temp.equals(w) & e.other(temp).equals(v)) {
				e.setTiempo(tiempo);
				e.setVelocidad(e.getCost()/tiempo);
				return;
			}
		}
	}

	public void addVertex(K idVertex, V infoVertex) {
		Vertex vertex = new Vertex(idVertex, infoVertex);
		if (!adj.contains(idVertex)) {
			V++;
		}
		adj.put(idVertex, vertex);
	}

	public Iterator<K> adj(K idVertex)
	{
		return adj.get(idVertex).adj();
	}
	
	public ArregloDinamico<Edge> adjEdges(K idVertex)
	{
		return adj.get(idVertex).getAdjacents();
	}

	public void uncheck() {
		Iterator<K> keys = adj.keys();
		while (keys.hasNext()) 
		{
			K k = (K) keys.next();
			Vertex tempV = adj.get(k);
			tempV.unmark();
		}
	}

	public void dfs(K s) 
	{
		adj.get(s).dfs();
	}

	public int cc() 
	{
		int numCC = 1;
		Iterator<K> keys = adj.keys();
		while (keys.hasNext()) 
		{
			K k = (K) keys.next();
			Vertex temp = adj.get(k);
			if (!temp.isMarked()) 
			{
				temp.markCC(numCC);
				numCC++;
			}
		}
		return numCC-1;
	}

	public Queue<K> getCC(K idVertex)
	{
		int cc = adj.get(idVertex).getCC();
		Queue<K> iter = new Queue<K>();
		Iterator<K> ver = adj.keys();
		while (ver.hasNext()) {
			K k = (K) ver.next();
			Vertex temp = adj.get(k);
			if (temp.getCC()== cc) iter.enqueue(k);
		}
		return iter;
	}
	
	public V getVerticeCC(int cc)
	{
		Iterator<K> ver = adj.keys();
		while (ver.hasNext()) {
			K k = (K) ver.next();
			Vertex<K,V> v = adj.get(k);
			if(v.getCC() == cc) return (V) v.getInfo();
		}
		return null;
	}
	
	public Queue<Integer> numCC()
	{
		Queue<Integer> cola=new Queue<Integer>();
		Iterator<K> keys = adj.keys();
		while (keys.hasNext()) 
		{
			K k = (K) keys.next();
			Vertex temp = adj.get(k);
			cola.enqueue(temp.getCC());
		}
		return cola;
	}
	
	public boolean isMarked(K id) 
	{
		return adj.get(id).isMarked();
	}
	

}

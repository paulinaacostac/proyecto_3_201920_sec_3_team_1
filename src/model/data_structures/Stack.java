package model.data_structures;


/**
 * @author leone
 * @version 1.0
 */
public class Stack<T> implements IStack<T> {

	private int size;
	private Node<T> top;
	
	private class Node<T>
	{
		private T elemento;
		private Node<T> siguiente;

		public Node(T pElemento)
		{
			elemento = pElemento;
			this.siguiente=null;
		}
		public Node<T> darSiguiente()
		{
			return siguiente;
		}
		public T darElemento() 
		{
			return elemento;
		}
		public void cambiarSiguiente(Node<T> nodo)
		{
			siguiente=nodo;
		}
	}

	public Stack(){
		size = 0;
		top = null;
	}
	
	public void push(T pItem){
		Node<T> newTop = new Node<T>(pItem);
		if (isEmpty()) 
		{
			top=newTop;
		}
		else
		{
			newTop.cambiarSiguiente(top);
			top=newTop;
		}
		size++;
	}

	public T pop()
	{
		T eliminado = null;
		if (!isEmpty()) 
		{
			eliminado = top.darElemento();
			top = top.darSiguiente();
			size--;
		}
		return eliminado;
	}

	public int getSize(){
		return size;
	}

	public boolean isEmpty(){
		return size==0;
	}

	public T getTop()
	{
		T topItem = null;
		if (!isEmpty()) 
			topItem = top.darElemento();	
		return topItem;
	}
}//end Stack
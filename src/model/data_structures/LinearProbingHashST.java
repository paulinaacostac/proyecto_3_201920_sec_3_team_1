package model.data_structures;

import java.util.Iterator;

public class LinearProbingHashST<Key extends Comparable<Key>, Value>
{
	private int N; 
	private int M;
	private Key[] keys; 
	private Queue<Value>[] values; 
	private int numRehash;
	
	public LinearProbingHashST()
	{
		N = 0;
		M= 5001;
		keys = (Key[]) new Comparable[M];
		values = (Queue<Value>[]) new Queue<?>[M];
	}
	
	public LinearProbingHashST(int cap)
	{
		N = 0;
		M= cap;
		keys = (Key[]) new Comparable[M];
		values = (Queue<Value>[]) new Queue<?>[M];
	}
	private int hash(Key key)
	{ 
		return (key.hashCode() & 0x7fffffff) % M; 
	}
	public int darNumRehash()
	{
		return numRehash;
	}
	private void resize(int cap)
	{
		LinearProbingHashST<Key, Value> t;
		t = new LinearProbingHashST<Key, Value>(cap);
		for (int i = 0; i < M; i++)
		{
			if (keys[i] != null)
			{
				while (!values[i].isEmpty()) 
				{
					t.putInSet(keys[i], values[i].dequeue());
				}
			}
		}
		keys = t.keys;
		values = t.values;
		M = t.M;
		numRehash++;
	}
	public void put(Key key, Value val)
	{
		
		int i;
		for (i = hash(key); keys[i] != null; i = (i + 1) % M)
		{
			if (keys[i].equals(key)) 
			{ 
				values[i] = new Queue<Value>();
				values[i].enqueue(val);
				return; 
			}
		}
		keys[i] = key;
		values[i] = new Queue<Value>();
		values[i].enqueue(val);
		N++;
		if ((double)N/(double)M > 0.75) resize(2*M); 
	}
	
	public void putInSet(Key key, Value val)
	{	
		int i;
		for (i = hash(key); keys[i] != null; i = (i + 1) % M)
		{
			if (keys[i].equals(key)) 
			{ 
				values[i].enqueue(val);
				return; 
			}
		}
		keys[i] = key;
		values[i] = new Queue<Value>();
		values[i].enqueue(val);
		N++;
		if ((double)N/(double)M > 0.75) resize(M*2); 
	}
	
	public Value get(Key key)
	{
		for (int i = hash(key); keys[i] != null; i = (i + 1) % M)
		{
			if (keys[i].equals(key))
			{
				return values[i].getFirst();
			}
		}
		return null;
	}
	
	public Iterator<Value> getSet(Key key)
	{
		for (int i = hash(key); keys[i] != null; i = (i + 1) % M)
		{
			if (keys[i].equals(key))
			{
				return values[i];
			}
		}
		return null;
	}
	
	public Value delete(Key key)
	{
		Value deleted = null;
		if (!contains(key)) return deleted;
		int i = hash(key);
		while (!key.equals(keys[i]))
		{
			i = (i + 1) % M;
		}
		deleted = values[i].getFirst();
		keys[i] = null;
		values[i] = null;
		i = (i + 1) % M;
		while (keys[i] != null)
		{
			Key keyToRedo = keys[i];
			Queue<Value> valsToRedo = values[i];
			keys[i] = null;
			values[i] = null;
			N--;
			while (!valsToRedo.isEmpty()) {
				Value valToRedo = valsToRedo.dequeue();
				putInSet(keyToRedo, valToRedo);
			}
			i = (i + 1) % M;
		}
		N--;
		return deleted;
	}
	
	public Iterator<Value> deleteSet(Key key)
	{
		Iterator<Value> deleted = null;
		if (!contains(key)) return deleted;
		int i = hash(key);
		while (!key.equals(keys[i]))
		{
			i = (i + 1) % M;
		}
		deleted = values[i];
		keys[i] = null;
		values[i] = null;
		i = (i + 1) % M;
		while (keys[i] != null)
		{
			Key keyToRedo = keys[i];
			Queue<Value> valsToRedo = values[i];
			keys[i] = null;
			values[i] = null;
			N--;
			while (!valsToRedo.isEmpty()) {
				Value valToRedo = valsToRedo.dequeue();
				putInSet(keyToRedo, valToRedo);
			}
			i = (i + 1) % M;
		}
		N--;
		return deleted;
	}
	public boolean contains(Key key)
	{
		return get(key) !=null;
	}
	public Iterator<Key> keys()
	{
		Queue<Key> keysM =  new Queue<Key>();
		for (int i = 0; i < keys.length; i++) 
		{
			if (keys[i] != null) keysM.enqueue(keys[i]);
		}
		return keysM;
	}
	public boolean isEmpty() 
	{
		return N == 0;
	}
//	public int nextPrime(int n)
//	{
//		int nextPrime = n;
//		if (n%2 == 0)
//			nextPrime = n+1;
//		else
//			nextPrime = n+2;
//		boolean found = false;
//		while (!found)
//		{
//			boolean isPrime = true;
//			for (int i = 2; i < nextPrime && isPrime; i++) 
//			{
//				if (nextPrime%i == 0) 
//				{
//					isPrime=false;
//				}
//			}
//			if (isPrime) 
//			{
//				found=true;
//			}else {
//				nextPrime+=2;
//			}
//		}
//		return nextPrime;
//	}
	public int darN()
	{
		return N;
	}
	
	public int darM()
	{
		return M;
	}
	
}

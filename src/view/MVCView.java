package view;

import model.data_structures.ArregloDinamico;
import model.data_structures.Esquina;
import model.logic.MVCModelo;
import model.logic.Viaje;
import model.logic.MVCModelo;
import model.logic.Viaje;
import model.logic.Zona;

public class MVCView 
{
	private MVCModelo modelo;
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{
		modelo = new MVCModelo();

	}

	public void printMenu()
	{
		System.out.println("1. Cargar grafo");
		System.out.println("2. Exportar grafo");
		System.out.println("3. Importar Grafo");
		System.out.println("4. Numero de componentes conectadas");
		System.out.println("5. Exportar grafo delimitado por coordenadas");
		System.out.println("6. Visualizar grafo en mapbox");
		System.out.println("7. Numero de vertices de 5 componentes conectadas mas grandes");
		System.out.println("8. Req 1A");
		System.out.println("9. Req 2A");
		System.out.println("10. Req 3A");
		System.out.println("11. Req 1B");
		System.out.println("12. Req 2B");
		System.out.println("13. Req 3B");
		System.out.println("14. Req 1C");
		System.out.println("15. Req 2C");
		System.out.println("16. Req 3C");
		System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
	}



	public void printCargar(int vertices, int arcos)
	{
		System.out.println("Numero de vertices cargados: "+vertices);
		System.out.println("Numero de arcos cargados: "+arcos);
	}
	public void printImportar(int vertices, int arcos)
	{
		System.out.println("Numero de vertices importados: "+vertices);
		System.out.println("Numero de arcos importados: "+arcos);

	}
	public void printConsultarRangoPorId(ArregloDinamico<Zona> ad)
	{
		for (int i=0;i<ad.darTamano();i++)
		{
			if (ad.darElemento(i)==null)
			{
				System.out.println("La zona con llave "+i+"no existe");
			}
			else
			System.out.println(" Sourceid: "+ad.darElemento(i).darSourceId()+" Nombre: "+ad.darElemento(i).darNombre()+" Perimetro: "+ad.darElemento(i).darPerimetro()+" Area: "+ad.darElemento(i).darArea()+" Numero Puntos: "+ad.darElemento(i).darNumeroCoordenadas());
		}
	}
	
	public void printDatosAnalisis(int pTotalNodos, int pAltura, double pAlturaHojas)
	{
		System.out.println("El n�mero total de nodos es: "+pTotalNodos);
		System.out.println("La altura real del �rbol es: "+pAltura);
		System.out.println("La altura promedio de las hojas es: "+pAlturaHojas);
	}

}

package controller;


import java.util.Iterator;
import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.data_structures.Graph;
import model.data_structures.Vertex;
import model.data_structures.Edge;
import model.data_structures.LinearProbingHashST;
import model.data_structures.MaxHeapCP;
import model.data_structures.Stack;
import model.logic.MVCModelo;
import model.logic.Vertice;
import model.logic.Viaje;
import view.MVCView;

public class Controller 
{

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo=new MVCModelo();
	}

	public void run() 
	{
		int tamanoInicial = 4001;
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		ArregloDinamico<Viaje> viajesHora=null;
		Integer dato1 = 0;
		Integer dato2 = 0;
		String respuesta = "";
		String respuesta1 = "";
		double startTime;
		double endTime;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("--------- \nCargando... ");
				modelo.cargarVertices();
				modelo.cargarViajes();
				modelo.cargarArcos();
				System.out.println("Archivo CSV cargado");
				view.printCargar(modelo.darGrafo().V(),modelo.darGrafo().E());
				break;
			case 2:
				System.out.println("--------- \nNombre del archivo a exportar");
				//modelo.exportarArcos();
				//modelo.exportarVertices();
				respuesta=lector.next();
				System.out.println("--------- \nExportando grafo");
				modelo.exportarGrafo(respuesta+".json");
				System.out.println("Grafo exportado");
				break;
			case 3:
				System.out.println("--------- \nImportar Grafo");
				System.out.println("--------- \nDar ruta de archivo de grafos (ej: pruebaVertices.json)");
				respuesta1=lector.next();
				modelo.importarGrafo(respuesta1+".json");
				System.out.println("Grafo importado");
				view.printImportar(modelo.darGrafo().V(),modelo.darGrafo().E());
				break;
			case 4:
				System.out.println("El numero de componentes conectadas es: "+modelo.componentesConectadas());
				break;
			case 5:
				System.out.println("--------- \nNombre del archivo a exportar");
				respuesta=lector.next();
				System.out.println("--------- \nExportando grafo delimitado");
				modelo.exportarGrafoDelimitado(-74.094723, -74.062707, 4.597714, 4.621360,respuesta+".json");
				System.out.println("Grafo delimitado exportado");
				break;
			case 6:
				System.out.println("Visualizar grafo delimitado en mapbox en el siguiente link:");
				System.out.println("https://api.mapbox.com/styles/v1/paulinaacosta/ck3ciw4vs19xw1cmk06fdezml.html?fresh=true&title=view&access_token=pk.eyJ1IjoicGF1bGluYWFjb3N0YSIsImEiOiJjazJ3b2FkazQwaGw1M21wZ2lsaGVnZjM4In0.DLMAb6ndFjq6nihqk2wAiA#17.3/4.610489/-74.089520/0");
				break;
			case 7:

				System.out.println("Numero de vertices de las 5 componentes conectadas mas grandes: ");
				MaxHeapCP<Integer> cola=modelo.darColaNumCCs();
				System.out.println(cola.sacarMax());
				System.out.println(cola.sacarMax());
				System.out.println(cola.sacarMax());
				System.out.println(cola.sacarMax());
				System.out.println(cola.sacarMax());
				break;
			case 8:
				System.out.println("Longitud origen:");
				double longitudO = lector.nextDouble();
				System.out.println("Latitud origen:");
				double latitudO= lector.nextDouble();
				System.out.println("Longitud destino:");
				double longitudD= lector.nextDouble();
				System.out.println("Latitud destino:");
				double latitudD= lector.nextDouble(); 
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();
				System.out.println("Exportando camino");
				Stack<Edge> path = modelo.darCaminoCostoMinimoTiempo(longitudO, latitudO, longitudD, latitudD,respuesta);
				System.out.println("Camino exportado");
				int num1A = modelo.darArcos1A()+1;
				System.out.println("El total vertices: " + num1A);
				System.out.println("El costo total distancia: " + modelo.darCostoDistancia1A());
				System.out.println("El costo total tiempo: " + modelo.darCostoTiempo1A());
				modelo.reiniciar();
				break;
			case 9:
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();
				System.out.println("N�mero de vertices");
				int nVer = lector.nextInt();
				Stack<Edge> velocidades  = modelo.verticesMenorVelocidadPromedio(nVer, respuesta);
				break;
			case 10:
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();	
				System.out.println("Exportando MST Prim");
				startTime = (double) System.currentTimeMillis();
				Stack<Edge> mstPrim=modelo.mstPrim(respuesta);
				endTime = (double) System.currentTimeMillis();
				double duracionPrim = (endTime-startTime);
				int numVertices = modelo.darContadorPrim()+1;
				System.out.println("MST Prim exportado");
				System.out.println("Costo total: "+modelo.darCostoTotalPrim());
				System.out.println("Duracion MST Prim (ms): "+duracionPrim);
				System.out.println("Vertices en mst: "+numVertices);
				System.out.println("Arcos en mst: "+modelo.darContadorPrim());
				break;
			case 11:
				System.out.println("Longitud origen:");
				double longitudO1 = lector.nextDouble();
				System.out.println("Latitud origen:");
				double latitudO1= lector.nextDouble();
				System.out.println("Longitud destino:");
				double longitudD1= lector.nextDouble();
				System.out.println("Latitud destino:");
				double latitudD1= lector.nextDouble(); 
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();
				System.out.println("Exportando camino");
				Stack<Edge> path1 = modelo.darCaminoCostoMinimoDistancia(longitudO1, latitudO1, longitudD1, latitudD1,respuesta);
				System.out.println("Camino exportado");
				int num1B = modelo.darArcos1B()+1;
				System.out.println("El total vertices: " + num1B);
				System.out.println("El costo total distancia: " + modelo.darCostoDistancia1B());
				System.out.println("El costo total tiempo: " + modelo.darCostoTiempo1B());
				modelo.reiniciar();
				break;
			case 12:
				System.out.println("Longitud Origen");
				double longitudO2 = lector.nextDouble();
				System.out.println("Latitud Origen");
				double latitudO2 = lector.nextDouble();
				System.out.println("Tiempo");
				double tiempo=lector.nextDouble();
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();
				System.out.println("Exportando vertices");
				//LinearProbingHashST<Integer, Double> st=modelo.verticesAlcanzablesEnTiempo(-74.06751221000002, 4.603916379999986, tiempo,respuesta);
				LinearProbingHashST<Integer, Double> st=modelo.verticesAlcanzablesEnTiempo(longitudO2, latitudO2, tiempo, respuesta);
				Iterator<Integer> iter=st.keys();
				System.out.println("Los vertices alcanzables desde "+"["+longitudO2+" , "+latitudO2+"]"+" son:");
				while (iter.hasNext())
				{
					int id=iter.next();
					Vertice v=(Vertice) modelo.darGrafo().getVertex(id).getInfo();
					System.out.println("Identificador: "+id+" Coordenadas: "+"["+v.darLongitud()+" , "+v.darLatitud()+"]"+" Tiempo a vertice: "+st.get(id));
				}
				System.out.println("Vertices exportados");
				break;
			case 13:
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();	
				System.out.println("Exportando MST");
				startTime = (double) System.currentTimeMillis();
				Stack<Edge> mstKruskal=modelo.kruskal(respuesta);
				endTime = (double) System.currentTimeMillis();
				double duracionKruskal = (endTime-startTime);
				int verticesKruskal = modelo.darContadorKruskal()+1;
				System.out.println("MST exportado");
				System.out.println("Costo total: "+modelo.darCostoTotalKruskal());
				System.out.println("Duracion MST Kruskal (ms): "+duracionKruskal);
				System.out.println("Vertices en mst: "+verticesKruskal);
				System.out.println("Arcos en mst: "+modelo.darContadorKruskal());
				break;
			case 14:
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();
				System.out.println("Exportando grafo zonas (10 min...)");
				Graph<Integer, Vertice> grafo=modelo.grafoZonas1();
				ArregloDinamico<Edge>edges=grafo.getEdges();
				Stack<Edge> stack=new Stack<Edge>();
				for (int i=0;i<edges.darTamano();i++)
				{
					Edge e=edges.darElemento(i);
					stack.push(e);
				}
				modelo.exportarCamino(stack, respuesta);
				System.out.println("Numero de vertices en grafo zonas: "+grafo.V());
				System.out.println("Numero de arcos en grafo zonas: "+grafo.E());
				break;
			case 15:
				System.out.println("Longitud origen:");
				double longitudO3 = lector.nextDouble();
				System.out.println("Latitud origen:");
				double latitudO3= lector.nextDouble();
				System.out.println("Longitud destino:");
				double longitudD3= lector.nextDouble();
				System.out.println("Latitud destino:");
				double latitudD3= lector.nextDouble(); 
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();
				System.out.println("Exportando camino");
				Stack<Edge> path8 = modelo.darCaminoCostoMinimoDistanciaZonas(longitudO3, latitudO3, longitudD3, latitudD3,respuesta);
				System.out.println("Camino exportado");
				int num2C = modelo.darArcos2C()+1;
				System.out.println("El total vertices: " + num2C);
				//System.out.println("El costo total distancia: " + modelo.darCostoDistancia1B());
				System.out.println("El costo total tiempo: " + modelo.darCostoTiempo2C());
				modelo.reiniciar();
				break;
			case 16:
				System.out.println("Longitud origen:");
				double longitudO5 = lector.nextDouble();
				System.out.println("Latitud origen:");
				double latitudO5= lector.nextDouble();
				System.out.println("Nombre del archivo a exportar");
				respuesta=lector.next();
				System.out.println("Exportando camino");
				Stack<Edge> path10 = modelo.darCaminoMasVerticesZonas(longitudO5, latitudO5,respuesta);
				System.out.println("Camino exportado");
				int num3C = modelo.darArcos3C();
				System.out.println("El total arcos: " + num3C);
				//System.out.println("El costo total distancia: " + modelo.darCostoDistancia1B());
				//System.out.println("El costo total tiempo: " + modelo.darCostoTiempo2C());
				modelo.reiniciar();
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
